# -*- coding: utf-8 -*-
from datetime import date
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning


class EmployeeTransfer(models.Model):
    _name = 'product.boq.type'
    _description = 'BOQ Type'
    _order = "id desc"

    name = fields.Char(string='BOQ Type', help='Give a name of BOQ Type', copy=False, default="/")

