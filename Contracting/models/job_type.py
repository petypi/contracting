# -*- coding: utf-8 -*-
from datetime import date
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning


class JobType(models.Model):
    _name = 'project.job.type'
    _description = 'JobType'
    _order = "id desc"

    name = fields.Char(string='Job Type', help='Give a name of Job Type', copy=False, default="/")
    code = fields.Char(string='Code', help='code', default=lambda obj:

                      obj.env['ir.sequence'].next_by_code('project.job.type'))
    description = fields.Text("Description")

