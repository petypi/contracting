from odoo import models, fields, api


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    boq_type = fields.Many2one("product.boq.type","BOQ Type")

class ProductProduct(models.Model):
    _inherit = 'product.product'

    boq_type = fields.Many2one("product.boq.type","BOQ Type")