# -*- coding: utf-8 -*-
from datetime import date
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning


class CustomerInquiry(models.Model):
    _name = 'customer.inquiry'
    _description = 'Customer Inquiry'
    _order = "id desc"

    customer = fields.Many2one("res.partner",string='Customer', help='Give Customer')
    client = fields.Many2one("res.partner", string='client', help='Give Client')
    name = fields.Char(string='Reference', help='code', default=lambda obj:

                      obj.env['ir.sequence'].next_by_code('customer.inquiry'))
    currency_id = fields.Many2one("res.currency","Currency")
    job_type = fields.Many2one("project.job.type", "Job Type")
    date = fields.Datetime("Date")
    submission = fields.Datetime("Submission")
    opportunity = fields.Many2one("crm.lead")
    area = fields.Char(placeholder="Area")
    address = fields.Char(placeholder="Address")
    city = fields.Char(placeholder="City")
    state = fields.Many2one("res.country.state",placeholder="state")
    country = fields.Many2one("res.country",placeholder="Countrey")
    address_name = fields.Char(string="Address Name",placeholder="address_name")





