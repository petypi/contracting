# -*- coding: utf-8 -*-
###################################################################################
#
#
###################################################################################
{
    'name': 'Contracting Module',
    'version': '13.0.1.0.0',
    'summary': 'Project Subcontracting and Contracting',
    'category': 'Generic Modules/Project',
    'author': 'Technovic Infotech',
    'maintainer': 'Technovic Infotech',
    'company': 'Technovic Infotech',
    'website': 'https://Technovic.Infotech.com',
    'depends': ['base',
                'project',
                'stock',
                'purchase',
                'sale',
                 'account',
                'hr',
                'hr_expense',
                'hr_payroll',
                'hr_timesheet',
                'product',
                'crm',
                'base'
                ],
    'data': [
        'views/product_boq_type.xml',
        'views/product_template.xml',
        'views/project_job_type.xml',
        'views/customer_inquiry.xml',
        'views/customer_inquiry_sequence.xml',
        'views/ir_sequence.xml',
        'security/ir.model.access.csv',
    ],
    'images': ['static/description/banner.jpg'],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
}
