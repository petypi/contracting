# -*- coding: utf-8 -*-

from odoo import models, fields, api,SUPERUSER_ID,_
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import odoo.addons.decimal_precision as dp
from datetime import datetime
from odoo.exceptions import UserError, ValidationError, MissingError, Warning

class MaterialPlanning(models.Model):
    _name = 'material.plan'
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result
        self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        self.description = self.product_id.name
    
    product_id = fields.Many2one(
        'product.product',
        string='Product'
    )
    description = fields.Char(
        string='Description'
    )
    product_uom_qty = fields.Integer(
        'Quantity',
        default=1.0
    )
    product_uom = fields.Many2one(
        'uom.uom',#product.uom
        'Unit of Measure'
    )
    material_task_id = fields.Many2one(
        'project.task',
        'Material Plan Task'
    )
    mrp_boms = fields.One2many(comodel_name='mrp.bom.sale.order',
                               inverse_name='sale_order', string='Mrp BoM',
                               copy=True)

    @api.depends('product_uom_qty', 'product_id')
    def _calc_stock(self):
        for line in self:
            v_stock = []
            r_stock = []
            for oline in line.mrp_boms:
                v_stock.append(oline.bom_line.product_id.virtual_available /
                               (oline.product_uom_qty))
                r_stock.append(oline.bom_line.product_id.qty_available /
                               (oline.product_uom_qty))
            line.virtual_stock = min(v_stock or [0])
            line.real_stock = min(r_stock or [0])

    virtual_stock = fields.Float(string='Virtual stock',
                                 compute='_calc_stock')
    real_stock = fields.Float(string='Stock', compute='_calc_stock')

    def product_id_change(
            self, pricelist, product, qty=0, uom=False, qty_uos=0,
            uos=False, name='', partner_id=False, lang=False, update_tax=True,
            date_order=False, packaging=False, fiscal_position=False,
            flag=False):
        res = super(SaleOrderLine, self).product_id_change(
            pricelist, product, qty=qty, uom=uom, qty_uos=qty_uos, uos=uos,
            name=name, partner_id=partner_id, lang=lang, update_tax=update_tax,
            date_order=date_order, packaging=packaging,
            fiscal_position=fiscal_position, flag=flag)
        mrp_bom_obj = self.env['mrp.bom']
        product_obj = self.env['product.product']
        product_id = product_obj.search([('id', '=', product)])
        mrp_bom = []
        mrp_bom = mrp_bom_obj.search([
            ('product_tmpl_id', '=', product_id.product_tmpl_id.id),
            ('type', '=', 'phantom')])
        order_lines = []
        for line in mrp_bom.bom_line_ids:
            order_line = {
                'bom_line': line.id,
                'product_uom_qty':
                    line.product_qty * qty,
            }
            order_lines.append(order_line)
        res['value'].update({'mrp_boms': ([(0, 0, oline)
                                           for oline in order_lines])})
        return res

class ConsumedMaterial(models.Model):
    _name = 'consumed.material'
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result
        self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        self.description = self.product_id.name
            
    
    product_id = fields.Many2one(
        'product.product',
        string='Product'
    )
    description = fields.Char(
        string='Description'
    )
    product_uom_qty = fields.Integer(
        'Quantity',
        default=1.0
    )
    product_uom = fields.Many2one(
        'uom.uom',#product.uom
        'Unit of Measure'
    )
    consumed_task_material_id = fields.Many2one(
        'project.task',
        string='Consumed Material Plan Task'
    )

class ProjectTask(models.Model):
    _inherit = 'project.task'
    

    @api.depends('picking_ids.requisition_line_ids')
    def _compute_stock_picking_moves(self):
        for rec in self:
            rec.ensure_one()
            for picking in rec.picking_ids:
                rec.move_ids = picking.requisition_line_ids.ids
    
    def total_stock_moves_count(self):
        for task in self:
            task.stock_moves_count = len(task.move_ids)
    
    def _compute_notes_count(self):
        for task in self:
            task.notes_count = len(task.notes_ids)
    
    picking_ids = fields.One2many(
        'material.purchase.requisition',
        'task_id',
        string='Stock Pickings'
    )
    date_start = fields.Datetime()
    date_end = fields.Datetime()
    move_ids = fields.Many2many(
        'material.purchase.requisition.line',
        compute='_compute_stock_picking_moves',
        store=True,
    )
    material_plan_ids = fields.One2many(
        'material.plan',
        'material_task_id',
        string='Material Plannings'
    )
    consumed_material_ids = fields.One2many(
        'consumed.material',
        'consumed_task_material_id',
        string='Consumed Materials'
    )
    stock_moves_count = fields.Integer(
        compute='total_stock_moves_count', 
        string='# of Stock Moves',
        store=True,
    )
    parent_task_id = fields.Many2one(
        'project.task', 
        string='Parent Task', 
        readonly=True
    )
    child_task_ids = fields.One2many(
        'project.task', 
        'parent_task_id', 
        string='Child Tasks'
    )
    notes_ids = fields.One2many(
        'note.note', 
        'task_id', 
        string='Notes',
    )
    notes_count = fields.Integer(
        compute='_compute_notes_count', 
        string="Notes"
    )
    job_number = fields.Char(
        string = "Job Number",
        copy = False,
    )
    bom_id = fields.Many2one("mrp.bom", "Bill Of Quantities")
    cost_sheet_id = fields.Many2one("job.costing.sheet")


    #@api.onchange("bom_id")
    def onhange_bom(self):
        self.material_plan_ids.unlink()
        mrp_bom_obj = self.env['mrp.bom']
        mrp_bom = []
        mrp_bom = self.bom_id
        order_lines = []
        for line in mrp_bom.bom_line_ids:
            order_line = {
                'material_task_id': line.id,
                'product_id':line.product_id.id,
                'product_uom_qty':
                    line.product_qty,

            }
            order_lines.append(order_line)

        self.update({'material_plan_ids': ([(0, 0, oline)
                                           for oline in order_lines])})


    
    @api.model
    def create(self,vals):
        number = self.env['ir.sequence'].next_by_code('project.task')
        vals.update({
            'job_number': number,
        })
        return super(ProjectTask, self).create(vals) 
    

    def view_stock_moves(self):
        for rec in self:
            stock_move_list = []
            for move in rec.move_ids:
                #stock_move_list.append(move.id)
                stock_move_list += move.requisition_id.delivery_picking_id.move_lines.ids
        result = self.env.ref('stock.stock_move_action')
        action_ref = result or False
        result = action_ref.read()[0]
        result['domain'] = str([('id', 'in', stock_move_list)])
        return result
        

    def view_notes(self):
        for rec in self:
            res = self.env.ref('odoo_job_costing_management.action_task_note_note')
            res = res.read()[0]
            res['domain'] = str([('task_id','in',rec.ids)])
        return res

    def action_subcontract(self):
        """
                                     This Method creates draft PO after purchase manager has approved Purchase request
                                     """
        view_id = self.env.ref('purchase.purchase_order_form')
        order_line = []
        name_obj = self.env['product.product'].search([('name','=',self.name)])
        if not name_obj:
            name_obj=self.env['product.product'].create({'name':self.name})
        for line in self:
            product = name_obj.name
            fpos = self.env['account.fiscal.position']
            if self.env.uid == SUPERUSER_ID:
                company_id = self.env.user.company_id.id
                taxes_id = fpos.map_tax(
                    name_obj.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id))
            else:
                taxes_id = fpos.map_tax(name_obj.supplier_taxes_id)

            product_line = (0, 0, {'product_id': name_obj.id,
                                   'state': 'draft',
                                   'product_uom': name_obj.uom_po_id.id,
                                   'price_unit': 0,
                                   'date_planned': datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                                   # 'taxes_id' : ((6,0,[taxes_id.id])),
                                   'product_qty': 1,
                                   'name': name_obj.name,

                                   })
            order_line.append(product_line)

        # vals = {
        #     'order_line' : order_line
        # }
        #
        # po = self.env['purchase.order'].create(vals)

        return {
            'name': _('New Quotation'),
            'type': 'ir.actions.act_window',
            'res_model': 'purchase.order',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'view_id': view_id.id,
            'views': [(view_id.id, 'form')],
            'context': dict(
                default_order_line=order_line,
                default_state='draft',
                default_project_id=self.project_id.id,
                default_partner_id=self.project_id.partner_id.id

            )
        }
