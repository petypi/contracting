# -*- coding: utf-8 -*-

from datetime import date

from odoo import models, fields, api, _
from odoo.exceptions import Warning

class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'

    boq_type = fields.Selection(string='BOQ Type',
                                   related='product_id.boq_type')
    cost = fields.Float(string='Cost',
                                   related='product_id.standard_price')

class MrpBom(models.Model):
    _inherit = 'mrp.bom'
    address = fields.Char(placeholder="Address")
    city = fields.Char(placeholder="City")
    states = fields.Many2one("res.country.state", placeholder="state")
    country = fields.Many2one("res.country", placeholder="Countrey")
    address_name = fields.Char(string="Address Name", placeholder="address_name")
    consultant = fields.Many2one("res.users")
    reviewer = fields.Many2one("res.users")
    contractor = fields.Many2one("res.partner")
    client = fields.Many2one("res.partner")
    customer = fields.Many2one("res.partner")
    view_estimation = fields.Boolean("View Estimation")
    currency_id = fields.Many2one("res.currency","Currency")


class MrpBomSaleOrder(models.Model):
    _name = 'mrp.bom.sale.order'

    bom_line = fields.Many2one(comodel_name='mrp.bom.line')
    product_id = fields.Many2one(related='bom_line.product_id',
                                 string='Product')
    product_uom = fields.Many2one(related='bom_line.product_id.uom_id',
                                  string='Unit of Measure ')
    product_uom_qty = fields.Float(string='Quantity (UoS)')
    sale_order = fields.Many2one(comodel_name='sale.order.line')

