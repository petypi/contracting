# -*- coding: utf-8 -*-

from odoo import models, fields

class AccountInvoiceLine(models.Model):
    _inherit = 'account.move.line'
    
    job_cost_id = fields.Many2one(
        'job.costing',
        string='Job Cost Center',
    )
    job_cost_line_id = fields.Many2one(
        'job.cost.line',
        string='Job Cost Line',
    )
    

class AccountInvoice(models.Model):
    _inherit = 'account.move'

    project_id = fields.Many2one("project.project", string="Project", required=True)
    job_id = fields.Many2one("project.task", string="Job order")
    cost_sheet_id = fields.Many2one("job.costing", "Cost Sheet")

    def _prepare_invoice_line_from_po_line(self, line):
        data = super(
            AccountInvoice, self
        )._prepare_invoice_line_from_po_line(line)
        data.update({
            'job_cost_id': line.job_cost_id.id,
            'job_cost_line_id': line.job_cost_line_id.id,
        })
        return data

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    project_id = fields.Many2one("project.project", string="Project")
    job_id = fields.Many2one("project.task", string="Job order")
    cost_sheet_id = fields.Many2one("job.costing.sheet", "Cost Sheet")

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    cost = fields.Float(string="Cost")

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    project_id = fields.Many2one("project.project", string="Project")
    job_id = fields.Many2one("project.task", string="Job order")
    cost_sheet_id = fields.Many2one("job.costing.sheet", "Cost Sheet")



