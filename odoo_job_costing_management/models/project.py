# -*- coding: utf-8 -*-

from odoo import fields, models, api, _

class Project(models.Model):
    _inherit = "project.project"

    
    job_cost_count = fields.Integer(

    )
    order_count =  fields.Integer(
        compute='_compute_order_count'
    )
    invoice_count = fields.Integer(
        compute='_compute_invoice_count'
    )
    payment_count = fields.Integer(
        compute='_compute_payment_count'
    )
    purchase_count = fields.Integer(
        compute='_compute_purchase_count'
    )
    shipment_count = fields.Integer(
        compute='_compute_shipment_count'
    )

    delvery_count = fields.Integer(
        compute='_compute_delivery_count'
    )
    

    invoicing_policy = fields.Boolean("Invoicing Policy")
    fixed_price = fields.Float()
    is_time_sheet = fields.Boolean
    advance_percentage = fields.Float("Advance Percentage(%)")
    retension_persentage = fields.Float("Retension Percentation(%)")
    inquiry_id = fields.Many2one("customer.inquiry", string="Customer Inquiry", required = True)
    adjusted_advance = fields.Float("Adjusted Advance")
    remaining_advance = fields.Float("Remaining Advance")
    adjusted_retension = fields.Float("Adjusted Retension")
    remaining_retension = fields.Float("Remaining Retension")
    qty_billing = fields.Boolean("Qty Billing")
    percentage_billing = fields.Boolean("Percentage Billing")


    

    def project_to_jobcost_action(self):
        job_cost = self.mapped('job_cost_ids')
        action = self.env.ref('odoo_job_costing_management.action_job_costing').read()[0]
        action['domain'] = [('id', 'in', job_cost.ids)]
        action['context'] = {'default_project_id':self.id,'default_analytic_id':self.analytic_account_id.id,'default_user_id':self.user_id.id}
        return action

    def _compute_order_count(self):
        order_count = self.env['sale.order']
        for task in self:
            task.order_count = order_count.search_count([('partner_id', '=', self.partner_id.id)])

    def _compute_invoice_count(self):
        invoice_count = self.env['account.move']
        for task in self:
            task.invoice_count = invoice_count.search_count([('partner_id', '=', self.partner_id.id)])

    def _compute_payment_count(self):
        payment_count = self.env['account.payment']
        for task in self:
            task.payment_count = payment_count.search_count([('partner_id', '=', self.partner_id.id)])

    def _compute_purchase_count(self):
        purchase_count = self.env['purchase.order']
        for task in self:
            task.purchase_count = purchase_count.search_count([('project_id', '=', self.id)])

    def _compute_shipment_count(self):
        shipment_count = self.env['stock.picking']
        for task in self:
            task.shipment_count = shipment_count.search_count([('project_id', '=', self.id),('picking_type_id.name','=','Receipts')])

    def _compute_delivery_count(self):
        delivery_count = self.env['stock.picking']
        for task in self:
            task.delvery_count = delivery_count.search_count([('partner_id', '=', self.partner_id.id),('picking_type_id.name','=','Delivery Orders')])


    def action_view_orders(self):
        orders = self.env['sale.order'].search([('partner_id','=',self.partner_id.id)])
        for order in orders:
            if not order.project_id:
                order.write({'project_id':self.id})
        return {
            'name': _('sale quotation'),
            'type': 'ir.actions.act_window',
            'domain': [('partner_id', '=', self.partner_id.id)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'sale.order',
            'view_id': False,

            'target': 'current',
            'context': {
                        'default_partner_id': self.partner_id.id}
        }

    def action_view_invoices(self):
        invoices = self.env['account.move'].search([('partner_id','=',self.partner_id.id)])
        for invoice in invoices:
            if not invoice.project_id:
                invoice.write({'project_id':self.id})
        return {
            'name': _('Invoices'),
            'type': 'ir.actions.act_window',
            'domain': [('partner_id', '=', self.partner_id.id)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,

            'target': 'current',
            'context': {
                        'default_partner_id': self.partner_id.id,
            'default_state':'draft'}
        }

    def action_view_payments(self):
        payments = self.env['account.payment'].search([('partner_id','=',self.partner_id.id)])
        for payment in payments:
            if not payments.project_id:
                payment.write({'project_id':self.id})
        return {
            'name': _('Payments'),
            'type': 'ir.actions.act_window',
            'domain': [('partner_id', '=', self.partner_id.id)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,

            'target': 'current',
            'context': {
                        'default_partner_id': self.partner_id.id,
            'default_state':'posted'}
        }

    def action_view_purchases(self):
        purchases = self.env['purchase.order'].search([('project_id', '=', self.id)])


        return {
            'name': _('Purchases'),
            'type': 'ir.actions.act_window',
            'domain': [('project_id', '=', self.id)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'purchase.order',
            'view_id': False,

            'target': 'current',
            'context': {
                'default_project_id': self.id,
               }
        }

    def action_view_shipments(self):
        shipments = self.env['stock.picking'].search([('picking_type_id.name', '=', 'Receipts'),('project_id', '=', self.id)])


        return {
            'name': _('Shipment'),
            'type': 'ir.actions.act_window',
            'domain': [('project_id', '=', self.id),('picking_type_id.name','=','Receipts')],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'view_id': False,

            'target': 'current',
            'context': {
                'default_project_id': self.id,
               }
        }

    def action_view_deliveries(self):
        deliveries = self.env['stock.picking'].search([('picking_type_id.name', '=', 'Delivery Orders'),('project_id', '=', self.id)])


        return {
            'name': _('Deliveries'),
            'type': 'ir.actions.act_window',
            'domain': [('picking_type_id.name', '=', 'Delivery Orders'),('partner_id','=',self.partner_id.id)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'view_id': False,

            'target': 'current',
            'context': {
                'default_project_id': self.id,
               }
        }

    @api.onchange("inquiry_id")
    def onchange_inquiry(self):
        for record in self:
            self.partner_id = self.inquiry_id.customer.id


class ProjectTask(models.Model):
    _inherit = 'project.task'
    

    def _compute_jobcost_count(self):
        jobcost = self.env['job.costing.sheet']
        job_cost_ids = self.mapped('job_cost_ids')
        for task in self:
            task.job_cost_count = jobcost.search_count([('id', '=', task.cost_sheet_id.id)])
    
    
    job_cost_count = fields.Integer(
        compute='_compute_jobcost_count'
    )
    
    job_cost_ids = fields.One2many(
        'job.costing.sheet',
        'task_id',
    )
    cost_sheet_id = fields.Many2one("job.costing.sheet", string="Job cost sheet")
    

    def task_to_jobcost_action(self):
        job_cost = self.mapped('job_cost_ids')
        action = self.env.ref('odoo_job_costing_management.action_job_costing_sheet').read()[0]
        action['domain'] = [('id', 'in', job_cost.ids)]
        action['context'] = {'default_task_id':self.id,'default_project_id':self.project_id.id,'default_analytic_id':self.project_id.analytic_account_id.id,'default_user_id':self.user_id.id}
        return action

    @api.onchange("cost_sheet_id")
    def update_from_costsheet(self):
        self.material_plan_ids.unlink()
        mrp_bom_obj = self.env['job.costing.sheet']
        mrp_bom = []
        mrp_bom = self.cost_sheet_id
        qty = mrp_bom.qty
        self.bom_id=mrp_bom.bom_code.id
        order_lines = []
        for line in mrp_bom.job_cost_ids:
            order_line = {
                'material_task_id': self.id,
                'product_id': line.product_id.id,
                'description': line.product_id.name,
                'product_uom_qty':
                    qty*(line.qty),

            }
            print(order_lines)
            order_lines.append(order_line)

        self.update({'material_plan_ids': ([(0, 0, oline)
                                       for oline in order_lines])})

#class ProjectIssue(models.Model):
#    _inherit = 'project.issue'
#    
#
#    def _compute_jobcost_count(self):
#        jobcost = self.env['job.costing']
#        job_cost_ids = self.mapped('job_cost_ids')
#        for task in self:
#            task.job_cost_count = jobcost.search_count([('id', 'in', job_cost_ids.ids)])
#    
#    
#    job_cost_count = fields.Integer(
#        compute='_compute_jobcost_count'
#    )
#    
#    job_cost_ids = fields.One2many(
#        'job.costing',
#        'issue_id',
#    )
#    
#
#    def issue_to_jobcost_action(self):
#        job_cost = self.mapped('job_cost_ids')
#        action = self.env.ref('odoo_job_costing_management.action_job_costing').read()[0]
#        action['domain'] = [('id', 'in', job_cost.ids)]
#        action['context'] = {'default_issue_id':self.id,'default_task_id':self.task_id.id,'default_project_id':self.project_id.id,'default_analytic_id':self.project_id.analytic_account_id.id,'default_user_id':self.user_id.id}
#        return action
        
        
        
