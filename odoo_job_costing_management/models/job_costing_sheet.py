# -*- coding: utf-8 -*-

from datetime import date

from odoo import models, fields, api, _
from odoo.exceptions import Warning


class JobCostingSheet(models.Model):
    _name = 'job.costing.sheet'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']  # odoo11
    #    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Job Costing sheet"
    _rec_name = 'number'

    def _compute_quotation_count(self):
        sale_count = self.env['sale.order']
        for task in self:

            task.quotation_count = sale_count.search_count([('cost_sheet_id', '=', task.id)])

    bom_code = fields.Many2one("mrp.bom", string="Bom Code", required=True)
    qty = fields.Float("Quantity", default=1)
    quotation_count = fields.Float("Quotation count", compute="_compute_quotation_count")
    uom_id = fields.Many2one("uom.uom", related="bom_code.product_id.uom_id", string="Unit of Measure")
    bom_total = fields.Float( string="Bom Cost")
    inquiry_id = fields.Many2one("customer.inquiry", string="Customer Inquiry Number", required=True)
    customer = fields.Many2one("res.partner", related="inquiry_id.customer", string="Customer")
    client = fields.Many2one("res.partner", related="inquiry_id.client", string="Client")
    other_details = fields.Text("Other Details")
    number = fields.Char("Cost Sheet Number", default=lambda obj:

    obj.env['ir.sequence'].next_by_code('job.costing.sheet'),readonly=True)
    job_cost_ids = fields.One2many(
        'job.costing.sheet.line',
        'job_cost_sheet_id',
        string='Job Costing sheets')
    product_uom_category_id = fields.Many2one("product.category")
    active = fields.Boolean(default=True)
    state = fields.Selection([("draft","draft"),("confirmed","confirmed"),("cancel","cancelled")], default='draft')
    task_id = fields.Many2one("project.task")



    @api.onchange("bom_code")
    def onhange_bom(self):
        print("####in onchange#####")
        self.job_cost_ids.unlink()
        mrp_bom_obj = self.env['mrp.bom']
        print(mrp_bom_obj)
        mrp_bom = []
        mrp_bom = self.bom_code
        order_lines = []
        qty=self.qty
        total=0
        for line in mrp_bom.bom_line_ids:
            total=total+ (line.product_id.standard_price*line.product_qty*qty)
            order_line = {
                'job_cost_sheet_id': self.id,
                'product_id': line.product_id.id,
                'qty':
                    line.product_qty*qty,
                'cost':line.product_id.standard_price*qty,
                'price':line.product_id.list_price*qty

            }
            print(order_lines)
            order_lines.append(order_line)
        self.bom_total = total
        print("total%s", total)

        self.update({'job_cost_ids': ([(0, 0, oline)
                                       for oline in order_lines])})


    @api.onchange("qty")
    def _get_estimates(self):
        total = 0
        qty = self.qty
        mrp_bom = self.bom_code
        for line in mrp_bom.bom_line_ids:
                total=total+ (qty * (line.product_id.standard_price))
        self.bom_total = total
        print("total%s",total)

    def approve(self):
        for record in self:
            self.write({'state':'confirmed'})

    def cancel(self):
        for record in self:
            self.write({'state':'cancel'})

    def unlink(self):
        for rec in self:
            if rec.state not in ('draft', 'cancel'):
                raise Warning(_('You can not delete Job Cost Sheet which is not draft or cancelled, cancel instead.'))
        return super(JobCostingSheet, self).unlink()

    def action_view_orders(self):
        cost_sheet = self.env['sale.order'].search([('cost_sheet_id','=',self.id)])
        if not cost_sheet:
            product = self.env['product.product'].search([('product_tmpl_id','=', self.bom_code.product_tmpl_id.id)])
            sale_vals = {

                'partner_id': self.customer.id,
                'cost_sheet_id': self.id,

                'order_line': [(0, 0, {
                    'name': self.bom_code.product_tmpl_id.name,
                    'price_unit': self.bom_total,
                    'product_uom_qty': 1.0,
                    'product_id': product.id,
                    'product_uom': self.bom_code.product_uom_id.id,
                })],
            }
            sale_order = self.env['sale.order'].create(sale_vals)
        return {
            'name': _('sale quotation'),
            'type': 'ir.actions.act_window',
            'domain': [('cost_sheet_id', '=', self.id)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'sale.order',
            'view_id': False,

            'target': 'current',
            'context': {'default_cost_sheet_id': self.id,
                        'default_partner_id': self.customer.id}
        }






# def _get_total_bom_cost(self):

class JobCostingsheetLine(models.Model):
    _name = 'job.costing.sheet.line'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']  # odoo11
    #    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Job Costing sheet line"
    _rec_name = 'product_id'

    product_id = fields.Many2one("product.product", string="Product")
    qty = fields.Float("Quantity")
    uom_id = fields.Many2one("uom.uom")
    cost = fields.Float(related="product_id.standard_price")
    price = fields.Float(related="product_id.list_price")
    margin = fields.Float( string="Margin", compute="_compute_margin")
    job_cost_sheet_id = fields.Many2one("job.costing.sheet", string="Job cost Sheet")
    mrp_boms = fields.One2many(comodel_name='mrp.bom.sale.order',
                               inverse_name='sale_order', string='Mrp BoM',
                               copy=True)
    product_uom_category_id = fields.Many2one("product.category")

    @api.depends("qty")
    def _compute_margin(self):
        for record in self:
            record.margin = record.price-record.cost
