# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ProjectSaleOrder(models.TransientModel):
    _name = "project.sale.order"
    _description = "project Sale Order"

    advance_payment_method = fields.Selection([
        ('delivered', 'Quotation'),

        ], string='Create ', default='delivered', required=True,
        help="")
    deduct_down_payments = fields.Boolean('Deduct down payments', default=True)
    has_down_payments = fields.Boolean('Has down payments', readonly=True)
    product_id = fields.Many2one('product.product', string='Down Payment Product', domain=[('type', '=', 'service')],
        )
    count = fields.Integer(string='Order Count')
    amount = fields.Float('Down Payment Amount', digits='Account', help="The percentage of amount to be invoiced in advance, taxes excluded.")
    currency_id = fields.Many2one('res.currency', string='Currency')
    fixed_amount = fields.Monetary('Down Payment Amount(Fixed)', help="The fixed amount to be invoiced in advance, taxes excluded.")
    deposit_account_id = fields.Many2one("account.account", string="Income Account", domain=[('deprecated', '=', False)],
        help="Account used for deposits")
    deposit_taxes_id = fields.Many2many("account.tax", string="Customer Taxes", help="Taxes used for deposits")

    @api.onchange('advance_payment_method')
    def onchange_advance_payment_method(self):
        if self.advance_payment_method == 'percentage':
            return {'value': {'amount': 0}}
        return {}


    def create_invoices(self):
        job_cost_sheet = self.env['job.costing.sheet'].browse(self._context.get('active_ids', []))
        order_lines = []
        for line in job_cost_sheet:
            product = self.env['product.product'].search([('product_tmpl_id', '=', line.bom_code.product_tmpl_id.id)])
            order_line = {
                'product_id': product.id,
                'name': product.name,
                'product_uom_qty':
                    line.qty,
                'price_unit':product.list_price,
                'cost':product.standard_price

            }
            print(order_lines)
            order_lines.append(order_line)
        partner=[]
        for item in job_cost_sheet:
            partner.append(item)
        partner_obj=partner[0]
        self.env['sale.order'].create({  'partner_id': partner_obj.customer.id,'cost_sheet_id':partner_obj.id,'order_line': ([(0, 0, oline)
                                            for oline in order_lines])})


        return {
            'name': _('sale quotation'),
            'type': 'ir.actions.act_window',
            'domain': [('cost_sheet_id', 'in', job_cost_sheet.ids)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'sale.order',
            'view_id': False,

            'target': 'current',
            'context': {}
        }
