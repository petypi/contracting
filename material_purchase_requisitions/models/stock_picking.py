# -*- coding: utf-8 -*-

from odoo import models, fields

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    custom_requisition_id = fields.Many2one(
        'material.purchase.requisition',
        string='Purchase Requisition',
        readonly=True,
        copy=True
    )
    project_id = fields.Many2one("project.project", string="Project",
                                 store=True, default=lambda self: self.custom_requisition_id.project_id.id)

    job_id = fields.Many2one('project.task', 'task',readonly=True, default=lambda self: self.custom_requisition_id.task_id.id)
    cost_sheet_id = fields.Many2one("job.costing.sheet", "Cost Sheet",default=lambda self: self.custom_requisition_id.cost_sheet_id.id)

class StockMove(models.Model):
    _inherit = 'stock.move'
    
    custom_requisition_line_id = fields.Many2one(
        'material.purchase.requisition.line',
        string='Requisitions Line',
        copy=True
    )

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
